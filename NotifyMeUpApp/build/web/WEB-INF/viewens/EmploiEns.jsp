<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NotifyMeUp</title>


     <link href="css/bootstrap.min.css" rel="stylesheet">
     <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>    
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
  
<script type="text/javascript" src="js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
       <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/mycss.css" rel="stylesheet">
    <script type="text/javascript" src="js/ens_js.js" ></script>
    <script type="text/javascript">
    $(document).ready(function() {
        //$('#emailgroupe').multiselect();
    });
    </script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                </button>
                <a class="navbar-brand" href="index.html"><img src="images/swp.png" width="180" height="25"></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                   <a href="#" class="toggle"><i class="fa fa-fw fa-gear"></i></a> 
                </li>
                <li class="dropdown">
                   <a href="#" class="toggle"><i class="fa fa-question-circle"></i></a> 
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><c:out value="${sessionScope.ens.nom}"></c:out> <c:out value="${sessionScope.ens.prenom}"></c:out><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-sign-out"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="index.html"><i class="fa fa-calendar "></i> Emplois du temps</a>
                    </li>        
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-9">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="font-weight: bold;">EMPLOI DU TEMPS <span id="datedujour">29/04/2017</span> Semaine  <span id="semaineactuelle"> 1</span></div>
                                    
                    
                        <div class="panel-body">
                            <div class="row">    
                               
                                 <!-- /.liste Semestre -->
                                 <div class="col-lg-2">
                                <label>Semaine</label>
                                <!--<select class="form-control" id="semaineselect" onfocus="this.size=5;" onblur="this.size=1;" onchange="this.size=1;this.blur();">-->
                                <select class="form-control" id="semaineselect">
                                    
                                 </select>
                                 </div>
                                <div class="col-lg-5">
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>8h-10h</th>
                                            <th>10h-12h</th>
                                            <th>12-14h</th>
                                            <th>14h-16h</th>
                                            <th>16h-18h</th>
                                            <th>18h-20h</th>
                                        </tr>
                                    </thead>
                                    <tbody class="myradio-group">
                                        <tr>
                                            <td>LUNDI</td>
                                            <td class="seance" id="11">
                                                <c:if test="${not empty seanceHashMap['lundi']['8']}">
                                                    <span id="idseance11" style="display: none;"><c:out value='${(seanceHashMap["lundi"])["8"].numS}'></c:out></span>
                                                    <!-- Enregistrer les ids des groupes et matieres dans des élements html invisibles pour après les réutiliser-->
                                                    <span id="matiereid11" style="display: none;"><c:out value='${(seanceHashMap["lundi"])["8"].numEmp.numM.codeM}'></c:out></span>
                                                   
                                                    <span id="matierenom11"><c:out value='${(seanceHashMap["lundi"])["8"].numEmp.numM.nomM}'></c:out></span>
                                                     <br/>
                                                     <span>Groupe</span>
                                                    <span id="groupeid11" style="display: none;"><c:out value='${(seanceHashMap["lundi"])["8"].numEmp.numG.numG}'></c:out></span>
                                                    <span id="groupenom11"><c:out value='${(seanceHashMap["lundi"])["8"].numEmp.numG.nomG}'></c:out></span>
                                                    <span id="niveau11"><c:out value='${(seanceHashMap["lundi"])["8"].numEmp.numG.niveau}'></c:out></span>
                                                    <span>Année</span>
                                                     <br/>
                                                    <span id="groupemail11" style="display: none;"><c:out value='${(seanceHashMap["lundi"])["8"].numEmp.numG.email}'></c:out></span>
                                                     
                                                    <span id="filierenom11"><c:out value='${(seanceHashMap["lundi"])["8"].numEmp.numG.nomFiliere}'></c:out></span>
                                                    <br/>
                                                    <span id="realdate11" style="display: none;"><c:out value='${(seanceHashMap["lundi"])["8"].numC.date}'></c:out></span>
                                                    <span id="heure11" style="display: none;">08h à 10h</span>
                                                    <span id="jour11" style="display: none;">Lundi</span>
                                                </c:if>
                                                
                                            </td>
                                            
                                            <td class="seance" id="12"></td>
                                            <td class="seance" id="13"></td>
                                            <td class="seance" id="14"></td>
                                        </tr>
                                        <tr>
                                            <td>MARDI</td>
                                            <td class="seance" id="21"></td>
                                            <td class="seance" id="22"></td>
                                            <td class="seance" id="23"></td>
                                            <td class="seance" id="24"></td>
                                        </tr>
                                        <tr>
                                            <td>MERCREDI</td>
                                            <td class="seance" id="31"></td>
                                            <td class="seance" id="32"></td>
                                            <td class="seance" id="33"></td>
                                            <td class="seance" id="34"></td>
                                        </tr>
                                         <tr>
                                            <td>JEUDI</td>
                                            <td class="seance" id="41"></td>
                                            <td class="seance" id="42"></td>
                                            <td class="seance" id="43"></td>
                                            <td class="seance" id="44"></td>
                                        </tr>
                                         <tr>
                                            <td>VENDREDI</td>
                                            <td class="seance" id="51"></td>
                                            <td class="seance" id="52"></td>
                                            <td class="seance" id="53"></td>
                                            <td class="seance" id="54"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">Paramètres Séance</div>
                        <div class="panel-body">
                            <div class="text-center">
                               <div class="btn-group-vertical" >
                                    <button type="button" class="btn btn-primary active"data-toggle="modal" data-target="#SigAbsc">Signaler Abscence</button>
                                    <br>
                                    <button type="button" class="btn btn-primary active"data-toggle="modal" data-target="#AjSeaSupp">Ajouter Scéance supplémentaire</button>
                                    <br>
                                    <button type="button" class="btn btn-primary active"data-toggle="modal" data-target="#Plann">Plannifier un Examen</button>
                                    <br>
                                </div>
                              
                                
                            </div>
                        </div>
                        </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">Legende</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div style="width:20px;height:10px;background-color:#6699FF;-moz-border-radius: 10px; "></div>
                                </div>
                                <div class="col-lg-10">
                                    <label><h6>Séance(s) Reportée(s)</h6></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div style="width:20px;height:10px;background-color:#66CC33;-moz-border-radius: 10px; "></div>
                                </div>
                                <div class="col-lg-10">
                                    <label><h6>Séance(s) Permutée(s)</h6></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div style="width:20px;height:10px;background-color:#FFAF37;-moz-border-radius: 10px; "></div>
                                </div>
                                <div class="col-lg-10">
                                    <label><h6>Séance(s) Supplémentaire(s) Ajoutée(s)</h6></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div style="width:20px;height:10px;background-color:#FFFF33;-moz-border-radius: 10px; "></div>
                                </div>
                                <div class="col-lg-10">
                                    <label><h6>Séance(s) d'Examen Ajoutée(s)</h6></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                                            
                                <div class="modal fade" id="SigAbsc" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <form action="signalerabscence" method="POST">
                                            <div class="modal-header">
                                                     <h4 class="modal-title">Signaler Abscence</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <label>A:</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="form-group">
                                                            <select class="form-control" id="emailgroupe" name="emailgroupe">
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input id="seancetoabsentid" type="text" style="display: none;" name="seancetoabsentid" value=""/>
                                                <input id="currentsemaineid" type="text" style="display: none;" name="currentsemaineid" value=""/>
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <label for="objetmessage">Objet:</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <!--<textearea rows="1" cols="50">Absence pour un cour</textarea>-->
                                                        <input type="text" id="objetmessage" name="objetmessage" class="form-control" value=""/>
                                                    </div>
                                                </div>
                                                <div class="row" style="padding-top: 10px;">
                                                    <div class="col-lg-2">
                                                   
                                                         <label for="message">Message</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <textarea id="message" class="form-control" rows="5" cols="200" style="text-align: left" name="message">
                                                          
                                                        </textarea>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <br>
                                                <div class="modal-footer">
                                                <button type="submit" class="btn btn-default">Envoyer</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </form>
                                            </div>
                                            
                                        </div>
                                    </div>
                                

                                 <div class="modal fade" id="AjSeaSupp" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                    <h4 class="modal-title">Ajouter Scéance supplémentaire</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                            <option>Mercredi: 8h-10h</option>
                                                            <option>Mercredi: 10h-12h</option>
                                                            <option>Mercredi: 14h-16h</option>
                                                            <option>Mercredi: 16h-18h</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                            <option>1ére Année</option>
                                                            <option>2éme Année</option>
                                                            <option>3émé Année</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                            <option>Informatique</option>
                                                            <option>Civil</option>
                                                            <option>Electrique</option>
                                                            <option>Mecanique</option>
                                                            <option>Reseau et Telecome</option>
                                                            <option>Minéral</option>
                                                            <option>Procédés industrielles</option>
                                                            <option>Modélisation des systèmes informatiques</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                            <option>All</option>
                                                            <option>GA</option>
                                                            <option>GB</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                            <option>UML et Pattern</option>
                                                            <option>Algorithmique</option>
                                                            <option>Innovation</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Ajouter</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="modal fade" id="Plann" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                    <h4 class="modal-title">Plannifier un Examen </h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                            <option>Mercredi: 8h-10h</option>
                                                            <option>Mercredi: 10h-12h</option>
                                                            <option>Mercredi: 14h-16h</option>
                                                            <option>Mercredi: 16h-18h</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                            <option>1ére Année</option>
                                                            <option>2éme Année</option>
                                                            <option>3émé Année</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                            <option>Informatique</option>
                                                            <option>Civil</option>
                                                            <option>Electrique</option>
                                                            <option>Mecanique</option>
                                                            <option>Reseau et Telecome</option>
                                                            <option>Minéral</option>
                                                            <option>Procédés industrielles</option>
                                                            <option>Modélisation des systèmes informatiques</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                            <option>All</option>
                                                            <option>GA</option>
                                                            <option>GB</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                            <option>UML et Pattern</option>
                                                            <option>Algorithmique</option>
                                                            <option>Innovation</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Ajouter</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

</body>

</html>
