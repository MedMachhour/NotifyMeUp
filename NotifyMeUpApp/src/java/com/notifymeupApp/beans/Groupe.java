/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.notifymeupApp.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author simo
 */
@Entity
@Table(name = "groupe")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Groupe.findAll", query = "SELECT g FROM Groupe g")
    , @NamedQuery(name = "Groupe.findByNumG", query = "SELECT g FROM Groupe g WHERE g.numG = :numG")
    , @NamedQuery(name = "Groupe.findByEmail", query = "SELECT g FROM Groupe g WHERE g.email = :email")
    , @NamedQuery(name = "Groupe.findByNiveau", query = "SELECT g FROM Groupe g WHERE g.niveau = :niveau")
        , @NamedQuery(name = "Groupe.findByNomFiliere", query = "SELECT g FROM Groupe g WHERE g.nomFiliere = :nomFiliere")
    , @NamedQuery(name = "Groupe.findByNumNiveau", query = "SELECT g FROM Groupe g WHERE g.numNiveau = :numNiveau")
    , @NamedQuery(name = "Groupe.findByNomG", query = "SELECT g FROM Groupe g WHERE g.nomG = :nomG")})
public class Groupe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "NumG")
    private Integer numG;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "Email")
    private String email;
    @Column(name = "Niveau")
    private Integer niveau;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NumNiveau")
    private int numNiveau;
    @Size(max = 255)
    @Column(name = "NomG")
    private String nomG;
     @OneToMany(mappedBy = "codeM")
    private List<Matiere> matiereList;
    @OneToMany(mappedBy = "numG")
    private List<Emp> empList;
    @OneToMany(mappedBy = "idGrp")
    private List<Etudiant> etudiantList;

    public Groupe() {
    }

    public Groupe(Integer numG) {
        this.numG = numG;
    }

    public Groupe(Integer numG, int numNiveau) {
        this.numG = numG;
        this.numNiveau = numNiveau;
    }

    public Integer getNumG() {
        return numG;
    }

    public void setNumG(Integer numG) {
        this.numG = numG;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getNiveau() {
        return niveau;
    }
    public void setNiveau(Integer niveau) {
        this.niveau = niveau;
    }

    public int getNumNiveau() {
        return numNiveau;
    }

    public void setNumNiveau(int numNiveau) {
        this.numNiveau = numNiveau;
    }

    public String getNomG() {
        return nomG;
    }

    public void setNomG(String nomG) {
        this.nomG = nomG;
    }

    @XmlTransient
    public List<Emp> getEmpList() {
        return empList;
    }

    public void setEmpList(List<Emp> empList) {
        this.empList = empList;
    }

    @XmlTransient
    public List<Etudiant> getEtudiantList() {
        return etudiantList;
    }

    public void setEtudiantList(List<Etudiant> etudiantList) {
        this.etudiantList = etudiantList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numG != null ? numG.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Groupe)) {
            return false;
        }
        Groupe other = (Groupe) object;
        if ((this.numG == null && other.numG != null) || (this.numG != null && !this.numG.equals(other.numG))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.notifymeupApp.beans.Groupe[ numG=" + numG + " ]";
    }

    public Object getNomFiliere() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
