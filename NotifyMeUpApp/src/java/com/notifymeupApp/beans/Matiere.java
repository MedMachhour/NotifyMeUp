/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.notifymeupApp.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author simo
 */
@Entity
@Table(name = "matiere")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Matiere.findAll", query = "SELECT m FROM Matiere m")
    , @NamedQuery(name = "Matiere.findByCodeM", query = "SELECT m FROM Matiere m WHERE m.codeM = :codeM")
    , @NamedQuery(name = "Matiere.findByNomM", query = "SELECT m FROM Matiere m WHERE m.nomM = :nomM")})
public class Matiere implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "CodeM")
    private String codeM;
    @Size(max = 255)
    @Column(name = "NomM")
    private String nomM;
    @OneToOne(mappedBy="numS")
    private Seance seance;
    @OneToMany(mappedBy = "numG")
    private List<Groupe> groupeList;
    @OneToMany(mappedBy = "numM")
    private List<Emp> empList;
    @OneToMany(mappedBy = "codeMatiere")
    private List<Etudiant> etudiantList;

    public Matiere() {
    }

    public Matiere(String codeM) {
        this.codeM = codeM;
    }

    public Matiere(String codeM, String nomM) {
        this.codeM = codeM;
        this.nomM = nomM;
    }

    

    public String getCodeM() {
        return codeM;
    }

    public void setCodeM(String codeM) {
        this.codeM = codeM;
    }

    public String getNomM() {
        return nomM;
    }

    public void setNomM(String nomM) {
        this.nomM = nomM;
    }

    @XmlTransient
    public List<Emp> getEmpList() {
        return empList;
    }

    public void setEmpList(List<Emp> empList) {
        this.empList = empList;
    }

    @XmlTransient
    public List<Etudiant> getEtudiantList() {
        return etudiantList;
    }

    public void setEtudiantList(List<Etudiant> etudiantList) {
        this.etudiantList = etudiantList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codeM != null ? codeM.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matiere)) {
            return false;
        }
        Matiere other = (Matiere) object;
        if ((this.codeM == null && other.codeM != null) || (this.codeM != null && !this.codeM.equals(other.codeM))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.notifymeupApp.beans.Matiere[ codeM=" + codeM + " ]";
    }
    
}
