/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.notifymeupApp.beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author simo
 */
public class Niveau implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "NumNiveau")
    private Integer numNiveau;

    public Niveau(Integer numNiveau) {
        this.numNiveau = numNiveau;
    }

    
    public Integer getNumNiveau() {
        return numNiveau;
    }

    public void setNumNiveau(Integer numNiveau) {
        this.numNiveau = numNiveau;
    }
    
}
