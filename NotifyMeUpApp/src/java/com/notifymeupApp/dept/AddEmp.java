/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.notifymeupApp.dept;



import com.notifymeupApp.beans.*;
import com.notifymeupApp.sessions.*;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.notifymeupApp.beans.Emp;

/**
 *
 * @author tlems
 */
@WebServlet(name = "AddEmp", urlPatterns = {"/addemp"})
public class AddEmp extends HttpServlet {

    @EJB
    EmpFacade empFacade;
    @EJB
    EnseignantFacade enseignantFacade;
    @EJB
    GroupeFacade groupeFacade;
    @EJB
    MatiereFacade matiereFacade;
    @EJB
    SemaineFacade semaineFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String empid = request.getParameter("emptoeditadd");
        String numerocasetoedit = request.getParameter("numerocase");
        String sd = request.getParameter("sd");
        String sf = request.getParameter("sf");
        String enseignant = request.getParameter("enseignant");
        String matiere = request.getParameter("matiere");
        String groupe = request.getParameter("groupe");
        String semestre = request.getParameter("semestre");
        System.out.println("empid = " + empid + "numerocase= " + numerocasetoedit + " sd = " + sd + " sf = " + sf + " enseignant = " + enseignant + " groupe = " + groupe);
        System.out.println("semestre = " + semestre);
        Date heure;
        Calendar cal = Calendar.getInstance();
        Integer num = Integer.parseInt(numerocasetoedit);
        if (num == 1 | num == 7 | num == 13 | num == 19 | num == 25 | num == 31) {
            cal.set(0, 0, 0, 8, 0, 0);
        }
        if (num == 2 | num == 8 | num == 14 | num == 20 | num == 26| num == 32) {
            cal.set(0, 0, 0, 10, 0, 0);
        }
        if (num == 3 | num == 9 | num == 15 | num == 21 | num == 27| num == 33) {
            cal.set(0, 0, 0, 12, 0, 0);
        }

        if (num == 4 | num == 10 | num == 16 | num == 22 | num == 28| num == 34) {
            cal.set(0, 0, 0, 14, 0, 0);
        }
        if (num == 5 | num == 11 | num == 17 | num == 23 | num == 29| num == 35) {
            cal.set(0, 0, 0, 16, 0, 0);
        }
        if (num == 6 | num == 12 | num == 18 | num == 24 | num == 30| num == 36) {
            cal.set(0, 0, 0, 18, 0, 0);
        }

        heure = cal.getTime();
        int jour = 2;
        if (num == 1 | num == 2 | num == 3 | num == 4|num == 5|num == 6) {
            jour = 2;
        }
        if (num == 7 | num == 8 | num == 9 | num == 10|num == 11|num == 12) {
            jour = 3;
        }
        if (num == 13 | num == 14 | num == 15 | num == 16|num == 17|num == 18) {
            jour = 4;
        }
        if (num == 19 | num == 20 | num == 21 | num == 22|num == 23|num == 24) {
            jour = 5;
        }
        if (num == 25 | num == 26 | num == 27 | num == 28|num == 29|num == 30) {
            jour = 6;
        }
        if (num == 31 | num == 32 | num == 33 | num == 34|num == 35|num == 36) {
            jour = 7;
        }

        Semaine sdd = semaineFacade.find(sd);
        Semaine sff = semaineFacade.find(sf);
        Enseignant ens = enseignantFacade.find(Integer.parseInt(enseignant));
        Matiere mat = matiereFacade.find(matiere);
        Groupe grp = groupeFacade.find(Integer.parseInt(groupe));

        if (empid.equals("")) {
            Emp emp = new Emp(1, jour, heure, sdd.getDatedebut(), sff.getDatedebut(), Integer.parseInt(semestre));
            emp.setNumG(grp);
            emp.setNumE(ens);
            emp.setNumM(mat);
            empFacade.create(emp);
            System.out.println("after creating emp");
            StringBuilder sb = new StringBuilder();
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.append("<emplois>");
            sb.append("<emploi>");
            sb.append("<idemploi>").append(emp.getNumEmp()).append("</idemploi>");
            sb.append("</emploi>");
            sb.append("</emplois>");
            response.setContentType("text/xml;charset=UTF-8");
            response.setHeader("Cache-Control", "no-cache");
            System.out.println("debut de sb" + sb.toString());
            response.getWriter().write(sb.toString());
            System.out.println("after setting response");

        } else {
            Emp emp = new Emp(Integer.parseInt(empid), jour, heure, sdd.getDatedebut(), sff.getDatedebut(), Integer.parseInt(semestre));
            emp.setNumG(grp);
            emp.setNumE(ens);
            emp.setNumM(mat);
            empFacade.edit(emp);
            System.out.println("after modifiying emp");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}