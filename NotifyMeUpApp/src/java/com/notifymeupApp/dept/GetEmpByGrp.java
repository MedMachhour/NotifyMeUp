/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.notifymeupApp.dept;

/**
 *
 * @author VAIO
 */
import com.notifymeupApp.beans.Emp;
import com.notifymeupApp.beans.EmpHashMap;
import com.notifymeupApp.beans.Groupe;
import com.notifymeupApp.sessions.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "GetEmpByGrp", urlPatterns = {"/getempbygrp"})
public class GetEmpByGrp extends HttpServlet {

    @EJB
    EmpFacade empFacade;
    @EJB
    GroupeFacade groupeFacade;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String groupeid = request.getParameter("grpid");
        System.out.println("grpid = " + groupeid);
        Integer grpid = Integer.parseInt(groupeid);
        String semestreid = request.getParameter("semestreid");
        Integer semstrid = Integer.parseInt(semestreid);
        Groupe grp = groupeFacade.find(grpid);
        List<Emp> listemp = empFacade.findByGrpAndSemestre(grp, semstrid);
        HashMap<String, HashMap<String, Emp>> empHashMap =  EmpHashMap.getEmpAsHashMap(listemp);
       
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
