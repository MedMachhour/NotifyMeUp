/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.notifymeupApp.sessions;

import com.notifymeupApp.beans.Emp;
import com.notifymeupApp.beans.Groupe;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author simo
 */
@Stateless
public class EmpFacade extends AbstractFacade<Emp> {

    @PersistenceContext(unitName = "NotifyMeUpAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmpFacade() {
        super(Emp.class);
    }
    public List<Emp> findByGrpAndSemestre(Groupe grp, int semestre) {
        TypedQuery<Emp> query =
        em.createQuery("SELECT e FROM Emp e WHERE e.idsemestre = :idsemestre AND e.numG = :numG", Emp.class);
        query.setParameter("idsemestre", semestre);
        query.setParameter("numG", grp);
        List<Emp> results = query.getResultList();
        return results;
    }
}
