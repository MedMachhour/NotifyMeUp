/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.notifymeupApp.sessions;

import com.notifymeupApp.beans.Seance;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author simo
 */
@Stateless
public class SeanceFacade extends AbstractFacade<Seance> {

    @PersistenceContext(unitName = "NotifyMeUpAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SeanceFacade() {
        super(Seance.class);
    }
    
}
